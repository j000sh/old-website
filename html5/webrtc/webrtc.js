function getMedia() {
    // first deal with browser prefixes
    var getUserMedia = navigator.getUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.webkitGetUserMedia;

    // make sure it's supported and bind to navigator
    if (getUserMedia) {
        getUserMedia = getUserMedia.bind(navigator);
    } else {
        // have to figure out how to handle the error somehow
    }

    // then deal with a weird, positional error handling API
    getUserMedia(
        // media constraints
        {video: true, audio: true},
        // success callback
        function (stream) {
            // gets stream if successful
        },
        // error callback
        function (error) {
            // called if failed to get media
        }
    )
}
