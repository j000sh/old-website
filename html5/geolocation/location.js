function initMap(mapId, infoId) {
	// http://developer.mapquest.com/web/documentation/plugins/leaflet/
    var map = L.map(mapId, {layers: MQ.mapLayer(), center: [43.084405, -77.674069], zoom: 15});
}

window.onload = function() {
    initMap("map", "info");
}
